package database

import (
	"context"
	"database/pkg/model"
	"errors"
	"fmt"
	"go.uber.org/zap"
)

type computeLayer interface {
	HandleQuery(ctx context.Context, input string) (model.Query, error)
}

type storageLayer interface {
	Get(ctx context.Context, key string) (string, bool)
	Set(ctx context.Context, key, value string) error
	Del(ctx context.Context, key string) error
}

type Database struct {
	computeLayer computeLayer
	storageLayer storageLayer
	logger       *zap.Logger
}

func NewDatabase(computeLayer computeLayer, storageLayer storageLayer, logger *zap.Logger) (*Database, error) {
	return &Database{
		computeLayer: computeLayer,
		storageLayer: storageLayer,
		logger:       logger,
	}, nil
}

func (db *Database) HandleQuery(ctx context.Context, input string) (string, error) {
	query, err := db.computeLayer.HandleQuery(ctx, input)
	if err != nil {
		db.logger.Info("error during query parsing", zap.Error(err))
		return "", err
	}
	db.logger.Info(fmt.Sprintf(query.String()))
	switch query.Command {
	case model.Get:
		return db.HandleGetQuery(ctx, query.Args)
	case model.Set:
		return db.HandleSetQuery(ctx, query.Args)
	case model.Del:
		return db.HandleDelQuery(ctx, query.Args)
	default:
		return "", errors.New("command unknown")
	}
}

func (db *Database) HandleGetQuery(ctx context.Context, args []string) (string, error) {
	v, ok := db.storageLayer.Get(ctx, args[0])
	if !ok {
		return "", errors.New("key not found")
	}
	return v, nil
}

func (db *Database) HandleSetQuery(ctx context.Context, args []string) (string, error) {
	err := db.storageLayer.Set(ctx, args[0], args[1])
	if err != nil {
		return "", err
	}
	return "ok", nil
}

func (db *Database) HandleDelQuery(ctx context.Context, args []string) (string, error) {
	err := db.storageLayer.Del(ctx, args[0])
	if err != nil {
		return "", err
	}
	return "ok", nil
}
