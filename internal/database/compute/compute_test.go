package compute

import (
	"context"
	"database/pkg/model"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
	"testing"
)

func TestNewCompute(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	parser := NewMockparser(ctrl)
	analyzer := NewMockanalyzer(ctrl)

	compute, err := NewCompute(nil, nil, nil)
	require.Error(t, err, "parser is nil")
	require.Nil(t, compute)

	compute, err = NewCompute(parser, nil, nil)
	require.Error(t, err, "analyzer is nil")
	require.Nil(t, compute)

	compute, err = NewCompute(parser, analyzer, nil)
	require.Error(t, err, "logger is nil")
	require.Nil(t, compute)

	compute, err = NewCompute(parser, analyzer, zap.NewNop())
	require.NoError(t, err)
	require.NotNil(t, compute)
}

func TestHandleQuery(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	controller := gomock.NewController(t)
	parser := NewMockparser(controller)
	parser.EXPECT().ParseQuery(ctx, "GET key").Return([]string{"GET", "key"}, nil)
	analyzer := NewMockanalyzer(controller)
	analyzer.EXPECT().AnalyzeQuery(ctx, []string{"GET", "key"}).
		Return(model.NewQuery(model.Get, []string{"key"}), nil)
	compute, err := NewCompute(parser, analyzer, zap.NewNop())
	require.NoError(t, err)

	query, err := compute.HandleQuery(ctx, "GET key")
	require.NoError(t, err)
	require.Equal(t, model.NewQuery(model.Get, []string{"key"}), query)
}

func TestHandleWrongCommandQuery(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	controller := gomock.NewController(t)
	parser := NewMockparser(controller)
	parser.EXPECT().
		ParseQuery(ctx, "DELETE *").
		Return(nil, invalidCommandError)
	analyzer := NewMockanalyzer(controller)

	compute, err := NewCompute(parser, analyzer, zap.NewNop())
	require.NoError(t, err)
	query, err := compute.HandleQuery(ctx, "DELETE *")
	require.Error(t, err, invalidCommandError)
	require.Equal(t, model.Query{}, query)
}
