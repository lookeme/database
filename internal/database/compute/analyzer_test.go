package compute

import (
	"context"
	"database/pkg/model"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
	"testing"
)

func TestNewAnalyzer(t *testing.T) {
	t.Parallel()

	analyzer, err := NewAnalyzer(nil)
	require.Error(t, err, "logger is nil")
	require.Nil(t, analyzer)

	analyzer, err = NewAnalyzer(zap.NewNop())
	require.NoError(t, err)
	require.NotNil(t, analyzer)
}

func TestAnalyzeQuery(t *testing.T) {
	tests := map[string]struct {
		tokens []string
		query  model.Query
		err    error
	}{
		"empty tokens": {
			tokens: []string{},
			err:    invalidCommandError,
		},
		"invalid command": {
			tokens: []string{"DELETE"},
			err:    invalidCommandError,
		},
		"invalid number arguments for set query": {
			tokens: []string{"SET", "key"},
			err:    argsNumberError,
		},
		"invalid number arguments for get query": {
			tokens: []string{"GET", "key", "value"},
			err:    argsNumberError,
		},
		"invalid number arguments for del query": {
			tokens: []string{"GET", "key", "value"},
			err:    argsNumberError,
		},
		"valid set query": {
			tokens: []string{"SET", "key", "value"},
			query:  model.NewQuery(model.Set, []string{"key", "value"}),
		},
		"valid get query": {
			tokens: []string{"GET", "key"},
			query:  model.NewQuery(model.Get, []string{"key"}),
		},
		"valid del query": {
			tokens: []string{"DEL", "key"},
			query:  model.NewQuery(model.Del, []string{"key"}),
		},
	}

	ctx := context.Background()
	analyzer, err := NewAnalyzer(zap.NewNop())
	require.NoError(t, err)

	for name, test := range tests {
		test := test
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			query, err := analyzer.AnalyzeQuery(ctx, test.tokens)
			require.Equal(t, test.query, query)
			require.Equal(t, test.err, err)
		})
	}
}
