package compute

import (
	"context"
	"database/pkg/model"
	"errors"
	"go.uber.org/zap"
	"strings"
)

var (
	loggerInitError     = errors.New("logger is nil")
	invalidCommandError = errors.New("invalid command")
	argsNumberError     = errors.New("wrong number of args")
)

type Analyzer struct {
	logger *zap.Logger
}

func NewAnalyzer(logger *zap.Logger) (*Analyzer, error) {
	if logger == nil {
		return nil, loggerInitError
	}

	return &Analyzer{
		logger: logger,
	}, nil
}

func (a *Analyzer) AnalyzeQuery(ctx context.Context, tokens []string) (model.Query, error) {
	if len(tokens) == 0 {
		return model.Query{}, invalidCommandError
	}
	command := stringToCommand(tokens[0])
	if command.Id == model.Unknown.Id {
		return model.Query{}, invalidCommandError
	}
	args := tokens[1:]
	if len(args) != command.ArgsNum {
		return model.Query{}, argsNumberError
	}
	return model.NewQuery(command, args), nil
}

func stringToCommand(str string) model.Command {
	str = strings.ToUpper(str)
	switch str {
	case model.Set.Id:
		return model.Set
	case model.Get.Id:
		return model.Get
	case model.Del.Id:
		return model.Del
	default:
		return model.Unknown
	}
}
