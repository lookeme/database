package compute

import (
	"context"
	"database/pkg/model"
	"errors"
	"go.uber.org/zap"
)

type parser interface {
	ParseQuery(context.Context, string) ([]string, error)
}

type analyzer interface {
	AnalyzeQuery(context.Context, []string) (model.Query, error)
}

type Compute struct {
	parser   parser
	analyzer analyzer
	logger   *zap.Logger
}

func NewCompute(parser parser, analyzer analyzer, logger *zap.Logger) (*Compute, error) {
	if parser == nil {
		return nil, errors.New("parser is nil")
	}

	if analyzer == nil {
		return nil, errors.New("analyzer is nil")
	}

	if logger == nil {
		return nil, errors.New("logger is nil")
	}

	return &Compute{
		parser:   parser,
		analyzer: analyzer,
		logger:   logger,
	}, nil
}

func (c *Compute) HandleQuery(ctx context.Context, queryStr string) (model.Query, error) {
	tokens, err := c.parser.ParseQuery(ctx, queryStr)
	if err != nil {
		return model.Query{}, err
	}
	query, err := c.analyzer.AnalyzeQuery(ctx, tokens)
	if err != nil {
		return model.Query{}, err
	}
	return query, nil
}
