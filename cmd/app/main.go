package main

import (
	"bufio"
	"context"
	"database/internal/database"
	"database/internal/database/compute"
	"database/internal/database/storage"
	"fmt"
	"go.uber.org/zap"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()
	logger, _ := zap.NewProduction()
	defer func(logger *zap.Logger) {
		err := logger.Sync()
		if err != nil {
			logger.Fatal("Error during defer logger", zap.Error(err))
		}
	}(logger)
	parser, err := compute.NewParser(logger)
	analyzer, err := compute.NewAnalyzer(logger)
	cmpt, _ := compute.NewCompute(parser, analyzer, logger)
	stge := storage.NewStorage()
	db, err := database.NewDatabase(cmpt, stge, logger)
	if err != nil {
		logger.Fatal("error during starting db", zap.Error(err))
	}
	logger.Info("db is up...")
	for {
		fmt.Println("please enter the command:")
		reader := bufio.NewReader(os.Stdin)
		line, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		val, err := db.HandleQuery(ctx, line)
		logger.Info("query", zap.String("result is ", val))
	}

}
