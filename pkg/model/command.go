package model

type Command struct {
	Id      string
	ArgsNum int
}

var (
	Unknown = Command{"", 0}
	Set     = Command{"SET", 2}
	Get     = Command{"GET", 1}
	Del     = Command{"DEL", 1}
)
