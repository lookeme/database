package model

import (
	"fmt"
)

type Query struct {
	Command Command
	Args    []string
}

func NewQuery(command Command, args []string) Query {
	return Query{
		Command: command,
		Args:    args,
	}
}

func (q *Query) String() string {
	return fmt.Sprintf("command: %s, args: %s", q.Command.Id, q.Args)
}
